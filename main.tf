provider "aws" {
  region = "us-east-1"
}

terraform {
    backend "s3" {
        bucket = "malcolm-terralock-example"
        key    = "lockexample/terraform.tfstate"
        region = "us-east-1"
        dynamodb_table = "terraform-state-lock-dynamo"
    }
}

resource "aws_instance" "Lock" {
  ami             = data.aws_ami.amazon_linux.id
  instance_type   = "t2.micro"
  key_name        = "malcolmjenkins"
  vpc_security_group_ids = ["sg-0bab97d6245061618"]
  tags = {
    Name = "Lock2"
  }
}